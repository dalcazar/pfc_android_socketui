package pfc.tripleA.asocketgui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class SocketActivity extends Activity {	
	private TabSpec tspec;
    private TabHost tabHost;
    private Button buttonConectar;
    private Button buttonDesconectar;
    private EditText editTextIP;
    private EditText editTextPuerto;
    private View viewStatusIcon;
    private TextView textViewStatusMessage;
    
    private TableLayout tableLayoutConversacion;
    private EditText editTextMensaje;
    private Button buttonEnviar;
    private ScrollView scrollViewConversacion;
    
    private EditText editTextURL;
    private Button buttonIrURL;
    private WebView webView;
    private Button buttonArriba;
    private Button buttonIzquierda;
    private Button buttonDerecha;
    private Button buttonAbajo;
    
    // TCP Components
    //private SSLSocket socket = null;
    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter out = null;
    
    private boolean esIpCorrecto;
    private boolean esPuertoCorrecto;    
    
    private final static int NULL = 0;
    private final static int DISCONNECTED = 1;
    private final static int DISCONNECTING = 2;
    private final static int BEGIN_CONNECT = 3;
    private final static int CONNECTED = 4;
    
    private final static String MOVER_ARRIBA = "0";
    private final static String MOVER_IZQUIERDA = "1";
    private final static String MOVER_DERECHA = "2";
    private final static String MOVER_ABAJO = "3";
    
    private static int connectionStatus = DISCONNECTED;
    
    public static final String PREFS_NAME = "MisPreferencias";    

	private static final int MY_NOTIFICATION_ID=1;
	private NotificationManager notificationManager;
	private Notification myNotification;
	
	private static int applicationStatus;
	private final static int RUNNING = 1;
	private final static int PAUSE = 2;
    
    public static StringBuffer toAppend = new StringBuffer("");
    public static StringBuffer toSend = new StringBuffer("");
    
    TextWatcher textWatcher = new TextWatcher() {
		
		public void onTextChanged(CharSequence s, int start, int before, int count) { }
		
		public void beforeTextChanged(CharSequence s, int start, int count,	int after) { }
		
		public void afterTextChanged(Editable s) { 
			UpdateButtonEnableState();
			//Log.i("ASocketGUIlog", "toggleleando el boton!!!; esIpCorrecto = " + esIpCorrecto + "; esPuertoCorrecto = " + esPuertoCorrecto);
		}
	};
	
	public void ShowToastNotification(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }
	
	public synchronized void SetApplicationStatus(int colorResId, int messageResId, int status) //Creo que no haria falta "synchronized" ya que a esta funcion solo se le llama desde el hilo de pintado (GUI), por lo que no puede haber carreras. Revisarlo. 
	{
		viewStatusIcon.setBackgroundResource(colorResId);
		textViewStatusMessage.setText(messageResId);
		connectionStatus = status;
		UpdateButtonEnableState();
	}
	
	public void UpdateButtonEnableState(){
		switch (connectionStatus) {
		case DISCONNECTED:
			if (esIpCorrecto && esPuertoCorrecto) {
				buttonConectar.setEnabled(true);
			} else {
				buttonConectar.setEnabled(false);
			}
			buttonDesconectar.setEnabled(false);
			break;
			
		case DISCONNECTING:
			buttonConectar.setEnabled(false);
			buttonDesconectar.setEnabled(false);
			break;

		case BEGIN_CONNECT:
			buttonConectar.setEnabled(false);
			buttonDesconectar.setEnabled(false);
			break;

		case CONNECTED:
			buttonConectar.setEnabled(false);
			buttonDesconectar.setEnabled(true);
			break;

		default:
			break;
		}
		if (esIpCorrecto && esPuertoCorrecto && (connectionStatus == DISCONNECTED)){
			buttonConectar.setEnabled(true);
			buttonDesconectar.setEnabled(false);
		}
	}
	
	public class ConnectSocket extends AsyncTask<Void, Void, Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			Boolean noError;
			try {
				//Me creo mi propio TrustManager para que no tire excepciones cuando falle la autentificacion
				TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() { return null; }
					public void checkClientTrusted(X509Certificate[] certs, String authType) { }
					public void checkServerTrusted(X509Certificate[] certs, String authType) { }
				} };
				
				SSLContext sslContext = SSLContext.getInstance("SSLv3");
				sslContext.init(null, trustAllCerts, new SecureRandom());
				SSLSocketFactory factory = sslContext.getSocketFactory();
				socket = (SSLSocket) factory.createSocket(editTextIP.getText().toString(), Integer.valueOf(editTextPuerto.getText().toString()));
				//socket = new Socket(editTextIP.getText().toString(), Integer.valueOf(editTextPuerto.getText().toString()));
				
				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
				noError = true;
			}
			// If error, clean up and output an error message
			catch (Exception e) {
				CleanUp();
				noError = false;
			}
			return noError;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				SetApplicationStatus(R.color.colorVerde, R.string.conectadoStatusMessageText, CONNECTED);
				new ManageConversation().execute();
			}
			else {
				SetApplicationStatus(R.color.colorRojo, R.string.errorConectandoStatusMessageText, DISCONNECTED);
			}
	    }
	}
    
	public class DisconnectSocket extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			CleanUp();
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			SetApplicationStatus(R.color.colorRojo, R.string.desconectadoStatusMessageText, DISCONNECTED);
	    }
	}
	
	public class ManageConversation extends AsyncTask<Void, Void, Void>{
		
		@Override
		protected void onPreExecute(){
			buttonEnviar.setEnabled(true);
			buttonArriba.setEnabled(true);
			buttonIzquierda.setEnabled(true);
			buttonDerecha.setEnabled(true);
			buttonAbajo.setEnabled(true);
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			String string;
			
			while ((connectionStatus == CONNECTED)) {
				try { // Poll every ~10 ms
					Thread.sleep(10);
				} catch (InterruptedException e) { }

				try {
					if (toSend.length() != 0) {
						out.print(toSend);
						out.flush();
						toSend.setLength(0);
						publishProgress();
					//}

					//if (in.ready()) { // NO FUNCIONA PARA LOS SSLSockets, siempre devuelve false
						string = in.readLine();
						if ((string != null) && (string.length() != 0)) {
							appendToChatBox("INCOMING: " + string + "\n");
							publishProgress();
						}
					}
				} catch (IOException e) { }
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			buttonEnviar.setEnabled(false);
			buttonArriba.setEnabled(false);
			buttonIzquierda.setEnabled(false);
			buttonDerecha.setEnabled(false);
			buttonAbajo.setEnabled(false);
			
	    }
		
		@Override
		protected void onProgressUpdate(Void... progress) {
			addRow(toAppend.toString());
			//editTextConversacion.append(toAppend.toString());
			scrollViewConversacion.postDelayed(new Runnable() {
				
				public void run() {
					scrollViewConversacion.fullScroll(ScrollView.FOCUS_DOWN);
				}
			}, 20);
			
			if (applicationStatus == PAUSE)
			{
				ShowNotification(toAppend);
			}
			toAppend.setLength(0);
	    }
	}
	
	public void addRow(String text){         
        TableRow row = new TableRow(this);
        TextView t = new TextView(this);
        //ImageView iv = new ImageView(this);
        
        /*Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
        int width=50;
        int height=50;
        Bitmap resizedbitmap=Bitmap.createScaledBitmap(bm, width, height, true);

        iv.setImageBitmap(resizedbitmap);*/
        
        t.setText(text);
        
        if (text.startsWith("OUTGOING: ")) {
        	t.setGravity(Gravity.RIGHT);
        	row.addView(t);
		} else {
			row.addView(t);
		}
        
        tableLayoutConversacion.addView(row,new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
	}

	public void ShowNotification(StringBuffer toAppendText) {
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		myNotification = new Notification(R.drawable.ic_launcher, toAppendText.toString(), System.currentTimeMillis());
				
		Context context = getApplicationContext();
		String notificationTitle = "Conversacion Socket";
	    String notificationText = "Hay nuevos mensajes";
	    Intent myIntent = new Intent(this, SocketActivity.class);
	    myIntent.setAction("android.intent.action.MAIN");
	    myIntent.addCategory("android.intent.category.LAUNCHER");
	    PendingIntent pendingIntent = PendingIntent.getActivity(SocketActivity.this, 0, myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
	    myNotification.defaults |= Notification.DEFAULT_SOUND;
	    myNotification.flags |= Notification.FLAG_AUTO_CANCEL;
	    myNotification.setLatestEventInfo(context, notificationTitle, notificationText, pendingIntent);
	    notificationManager.notify(MY_NOTIFICATION_ID, myNotification);		
	}
	
	private static void appendToChatBox(String s) {
		synchronized (toAppend) {
			toAppend.append(s);
		}
	}

	private static void sendString(String s) {
		synchronized (toSend) {
			toSend.append(s + "\n");
		}
	}
	
    private void CleanUp() {
        try {
           if (socket != null) {
              socket.close();
              socket = null;
           }
        }
        catch (IOException e) { socket = null; }

        try {
           if (in != null) {
              in.close();
              in = null;
           }
        }
        catch (IOException e) { in = null; }

        if (out != null) {
           out.close();
           out = null;
        }
     }
 
    /** Called when the activity is first created. */
	
    @Override
    public void onCreate(Bundle savedInstanceState) {        
    	super.onCreate(savedInstanceState);

    	applicationStatus = RUNNING;
    	
        setContentView(R.layout.activity_socket);
 
        tabHost=(TabHost)findViewById(android.R.id.tabhost);        
        tabHost.setup();
        
        Resources res = getResources() ;
         
        tspec=tabHost.newTabSpec("Tab1");     
        tspec.setContent(R.id.tabConfiguracion);
        tspec.setIndicator(res.getText(R.string.tabConfiguracion)); 
        tabHost.addTab(tspec);
         
        tspec=tabHost.newTabSpec("Tab2");
        tspec.setContent(R.id.tabConversacion);
        tspec.setIndicator(res.getText(R.string.tabConversacion));
        tabHost.addTab(tspec);

        tspec=tabHost.newTabSpec("Tab3");
        tspec.setContent(R.id.tabControl);
        tspec.setIndicator(res.getText(R.string.tabControl));
        tabHost.addTab(tspec);
        
        //Log.i("ASocketGUIlog", "Lanzando la aplicacion.");
        
        viewStatusIcon = (View)findViewById(R.id.viewStatusIcon);
        
        textViewStatusMessage = (TextView)findViewById(R.id.textViewStatusMessage);
        textViewStatusMessage.setText(R.string.desconectadoStatusMessageText);
        
        buttonConectar = (Button) findViewById(R.id.buttonConectar);
        buttonDesconectar = (Button) findViewById(R.id.buttonDesconectar);

        buttonConectar.setEnabled(false);
        buttonDesconectar.setEnabled(false);
        
        buttonConectar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				ConnectivityManager conMgr =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		        
		        if ( conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED 
		            ||  conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTING  ) {
		        	SetApplicationStatus(R.color.colorNaranja, R.string.conectandoStatusMessageText, BEGIN_CONNECT);
		        	new ConnectSocket().execute();
		        }
		        else if ( conMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.DISCONNECTED 
		            ||  conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.DISCONNECTED) {
		        	ShowToastNotification(R.string.noHayConexionInternetText);
		        }
			}
		});
        
        buttonDesconectar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				SetApplicationStatus(R.color.colorNaranja, R.string.desconectandoStatusMessageText, DISCONNECTING);
				new DisconnectSocket().execute();
			}
		});
        
        editTextIP = (EditText)findViewById(R.id.editTextIP);
        editTextPuerto = (EditText)findViewById(R.id.editTextPuerto);
        
        InputFilter[] ipFilters = new InputFilter[1];
        ipFilters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            	//Log.i("ASocketGUIlog", "Validando IP. source = " + source + "; start = " + start + "; end = " + end + "; dest = " + dest + "; dstart = " + dstart + "; dend = " + dend);
            	String resultingTxt = "";
            	
                if (end > start) {
                    resultingTxt = dest.toString().substring(0, dstart) + source.subSequence(start, end) + dest.toString().substring(dend);
                    //Log.i("ASocketGUIlog", "Primer if");
                }
                else if ((start == end) && (start == 0) && (dest.length() > 0) && (source == "")){
                	resultingTxt = dest.subSequence(0, dest.length() - 1).toString();
                	//Log.i("ASocketGUIlog", "Segundo if; (source == \"\") = " + (source == "") + "; (source == null) = " + (source == null));
                }
                else if (!(source == "")){
                	return null;
                	//Log.i("ASocketGUIlog", "Ultimo if; source = " + source + "; dest.lenght = " + dest.length() + "; (source == \"\") = " + (source == "") + "; (source == null) = " + (source == String.));
                }
                
                if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
                	esIpCorrecto = false;
                    return "";
                } else {
                    String[] splits = resultingTxt.split("\\.");
                    esIpCorrecto = (splits.length == 4);
                    for (int i=0; i<splits.length; i++) {
                        if (Integer.valueOf(splits[i]) > 255) {
                        	esIpCorrecto = false;
                            return "";
                        }
                    }
                }
            return null;
            }
        };
        editTextIP.setFilters(ipFilters);
        
        InputFilter[] portFilters = new InputFilter[1];
        portFilters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            	//Log.i("ASocketGUIlog", "Validando Puerto. source = " + source + "; start = " + start + "; end = " + end + "; dest = " + dest + "; dstart = " + dstart + "; dend = " + dend);
            	String resultString = dest.toString();
            	
            	if ((start == end) && (start == 0) && (dest.length() > 0)) {
            		resultString = resultString.substring(0, resultString.length() - 1); 
				} else {
					resultString = dest.toString() + source;
				}
            	//Log.i("ASocketGUIlog", "resultString = " + resultString);
            	try {
	            	esPuertoCorrecto = ((Integer.valueOf(resultString) > 0) && (Integer.valueOf(resultString) < 65535));
            	} catch (NumberFormatException e) {
            		//Log.i("ASocketGUIlog", "e = " + e);
            		esPuertoCorrecto = false;
				}
            	
            	//Log.i("ASocketGUIlog", "esPuertoCorrecto = " + esPuertoCorrecto);
            	if (!esPuertoCorrecto)
            	{
            		return "";
            	}
            return null;
            }
        };
        editTextPuerto.setFilters(portFilters);
        
        editTextIP.addTextChangedListener(textWatcher);
        editTextPuerto.addTextChangedListener(textWatcher);
        
        editTextMensaje = (EditText)findViewById(R.id.editTextMensaje);
        buttonEnviar = (Button)findViewById(R.id.buttonEnviar);
        
        buttonEnviar.setEnabled(false);
        buttonEnviar.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				String string = editTextMensaje.getText().toString();
				if (!string.equals("")) {
					appendToChatBox("OUTGOING: " + string + "\n");
					editTextMensaje.setText("");
					
					sendString(string);
				}
			}
		});
        
        scrollViewConversacion = (ScrollView)findViewById(R.id.scrollViewConversacion);
        
        tableLayoutConversacion = (TableLayout) findViewById(R.id.tableLayoutConversacion);
        
        editTextURL = (EditText)findViewById(R.id.webViewUrl);
        webView = (WebView)findViewById(R.id.webview);
        buttonIrURL = (Button)findViewById(R.id.buttonCargarUrl);
        buttonIrURL.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				webView.getSettings().setJavaScriptEnabled(true);
				webView.loadUrl(editTextURL.getText().toString());
				//String customHtml = "<html><body><h1>Hello, WebView</h1></body></html>";
				//webView.loadData(customHtml, "text/html", "UTF-8");
			}
		});
        
        buttonArriba = (Button)findViewById(R.id.buttonArriba);
        buttonArriba.setEnabled(false);
        buttonArriba.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				sendString(MOVER_ARRIBA);				
			}
		});
        buttonIzquierda = (Button)findViewById(R.id.buttonIzquierda);
        buttonIzquierda.setEnabled(false);
        buttonIzquierda.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				sendString(MOVER_IZQUIERDA);	
			}
		});
        buttonDerecha = (Button)findViewById(R.id.buttonDerecha);
        buttonDerecha.setEnabled(false);
        buttonDerecha.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				sendString(MOVER_DERECHA);	
			}
		});
        buttonAbajo = (Button)findViewById(R.id.buttonAbajo);
        buttonAbajo.setEnabled(false);
        buttonAbajo.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				sendString(MOVER_ABAJO);	
			}
		});
        
        
        SetApplicationStatus(R.color.colorRojo, R.string.desconectadoStatusMessageText, DISCONNECTED);  
    }
        
    @Override
    protected void onPause() {
    	applicationStatus = PAUSE;
    	Log.i("appstatus", "en pausa");
    	super.onPause();
    }
    
    @Override
    protected void onResume() {
    	applicationStatus = RUNNING;
    	Log.i("appstatus", "running");
    	super.onResume();
    }
    
    public class DataReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			
		}
    	
    }

}